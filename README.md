# Home Assistant Add-ons: DUCKDNS FORK

## Add-ons provided by this repository

- **[Duck DNS with IPV6](/duckdns/README.md)**

    Automatically update your Duck DNS IP address with integrated HTTPS support via Let's Encrypt.
    (With better IPV6 functionality).

## Support

Got questions?
You have several options to get them answered:

- The [Home Assistant Discord Chat Server][discord].
- The Home Assistant [Community Forum][forum].
- Join the [Reddit subreddit][reddit] in [/r/homeassistant][reddit]

In case you've found a bug, please [open an issue on GitHub][issue].
